// CRUD OPERATIONS

/*
- CRUD operations are the heart of any backend application
- CRUD stands for Create, Read, Update, and Delete
- MongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
*/

// CREATE: Inserting documents

// INSERT ONE
/*
Syntax:
	db.collectionName.insertOne({})

Inserting/Assigning values in JS Objects:
	object.object.method({object})
*/

db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
});



// INSERT MANY
/*
Syntax:
	db.users.insertMany([
		{objectA},
		{objectB}
	])
*/

db.users.insertMany ([
{
	firstName: "Stephen",
	lastName: "Hawking",
	age: 76,
	contact: {
		phone: "578531212",
		email: "stephenhawking@gmail.com"
	},
	courses: ["Python", "React", "PHP"],
	department: "none"

},
{
	firstName: "Neil",
	lastName: "Armstrong",
	age: 82,
	contact: {
		phone: "578523252",
		email: "neilarmstrong@gmail.com"
	},
	courses: ["React", "Laravel", "SASS"],
	department: "none"

},

]);


// READ: FINDING DOCUMENTS

// FIND ALL
/*
Syntax:

	db.collectionName.find();

*/

db.users.find();

// Finding users with single arguments
/*
Syntax:
	db.collectionName.find({field: value})
*/
// Look for Stephen Hawking using firstName

db.users.find({firstName: "Stephen"});


// Finding users with multiple arguments
// with no matches
db.users.find({firstName: "Stephen", age: 20}); // 0 records - no match

// with matching one
db.users.find({firstName: "Stephen", age: 76, lastName: "Hawking"});


// UPDATE: Updating documents
// Repeat Jane to be updated
db.users.insert({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "5875698",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
});

// Update One
/*
Syntax:
	db.collectionName.updateOne({criteria} , {$set: {field: value}})
*/
db.users.updateOne(
	{firstName: "Jane"},
	{
		$set: {
			firstName: "Jane",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "000000",
				email: "janegates@gmail.com"
			},
			courses: ["AWS", "Google Cloud", "Azure"],
			department: "none",
			status: "active"
		}
	}
);

db.users.find({firstName: "Jane"})


// Update Many

	db.collectionName.updateMany(
		{department: "none"},
		{
			$set: {
				department: "HR"
			}
		}
	)

	db.users.find();

// REPLACE ONE
/*
- Can be used if replacing the whole document is necessary
- Syntax:
	db.collectionName.replaceOne({criteria}, {field: value}})
*/

	db.users.replaceOne(
		{ lastName: "Gates"},
			{
				firstName: "Bill",
				lastName: "Clinton"
			}
		)
	db.users.find({firstName: "Bill"});


// DELETE: Deleting Documents
db.users.insert({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});

db.users.find({firstName: "Test"});

// Deleting a single document
/*
Syntax:
	db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({firstName: "Test"});
db.users.find({firstName: "Test"});


// Delete many
/*
Syntax:
	db.collectionName.deleteMany({criteria})
*/

db.users.insertMany([
{
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
},
{
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact: {
		phone: "0000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
}
]);

db.users.deleteMany({courses: []}); //Will delete documents that has criteria of courses: []

// ADVANCED QUERIES

// Query an embedded document

db.users.find({
	contact : {
        phone : "5875698",
        email : "janedoe@gmail.com"
    }
});

// Find the document with the email "janedoe@gmail.com"
// Querying on nested fields
db.users.find({"contact.email": "janedoe@gmail.com"})


// Querying an Array with exact elements 
db.users.find({courses: [
		"React",
		"Laravel",
		"SASS"
	]})

// Quering an element without regard to order
db.users.find({courses: {$all: ["React", "SASS", "Laravel"]}});


// Make an array to query
db.users.insert({
	namearr: [
			{
				namea: "juan"
			},
			{
				nameb: "tamad"
			}

		]
})

db.users.find({
	namearr: {
		namea: "juan"
	}
});